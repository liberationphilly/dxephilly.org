DxE Philadelphia Website
========================
Website for Direct Action Everywhere Philadelphia. This repo is the front-end code, while some dynamic functionality is powered by [dxephilly-api](https://github.com/directactioneverywhere/dxephilly-api).

Local Development
-----------------
This site runs on a very simple Node.js app server using [Express](http://expressjs.com/) with [Harp](http://harpjs.com/) as middleware. Install Node.js and use `npm run start` to start the server.

Deploying
---------
This app is deployed with Dokku. [Learn about](https://github.com/directactioneverywhere/dxe-learn2dokku) how DxE Tech deploys with Dokku. The Dokku git remote is:

    dokku@dxephilly.org:dxephilly.org

License
=======
dxephilly.org is licensed under GNU GPL version 3.0. For the full license see the LICENSE file.
